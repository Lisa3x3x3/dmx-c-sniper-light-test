﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sniper.Lighting.DMX;

namespace SniperLightTest2
{
    static class Program
    {
        //var test = DmxController<DMXProUSB>();
        //DmxController<DMXProUSB> test = new DmxController<DMXProUSB>();
        static DMXProUSB dmx;

        static void Main(string[] args)
        {
            dmx = new DMXProUSB();
            dmx.start();
            Guid queueGuid = new Guid();
            var queueBuffer = dmx.CreateQueue(queueGuid, 1);
            

            var isConnected = dmx.Connected;
            Console.WriteLine(isConnected);
            
            //FadeThroughColors(queueGuid);

            while (true)
            {
                FlashLightningSeizure(queueGuid);
                System.Threading.Thread.Sleep(20000);
            }

            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();

            dmx.stop();
        }

        public static void FlashLightningSeizure(Guid queueGuid)
        {           
            dmx.SetDmxValue(0, 255, queueGuid, 1);
            dmx.SetDmxValue(1, 255, queueGuid, 1);
            dmx.SetDmxValue(2, 255, queueGuid, 1);
            dmx.SetDmxValue(7, 255, queueGuid, 1);

            System.Threading.Thread.Sleep(60);

            dmx.SetDmxValue(0, 0, queueGuid, 1);
            dmx.SetDmxValue(1, 0, queueGuid, 1);
            dmx.SetDmxValue(2, 0, queueGuid, 1);
            dmx.SetDmxValue(7, 0, queueGuid, 1);
            
        }

        public static void FadeThroughColors(Guid queueGuid)
        {
            for (byte i = 0; i < 255; i++)
            {
                dmx.SetDmxValue(0, 0, queueGuid, 1);
                dmx.SetDmxValue(1, i, queueGuid, 1);
                dmx.SetDmxValue(2, 0, queueGuid, 1);
                dmx.SetDmxValue(7, 255, queueGuid, 1);

                System.Threading.Thread.Sleep(10);
            }

            for (byte i = 0; i < 255; i++)
            {
                dmx.SetDmxValue(0, i, queueGuid, 1);
                dmx.SetDmxValue(1, 0, queueGuid, 1);
                dmx.SetDmxValue(2, 0, queueGuid, 1);
                dmx.SetDmxValue(7, 255, queueGuid, 1);

                System.Threading.Thread.Sleep(10);
            }

            for (byte i = 0; i < 255; i++)
            {
                dmx.SetDmxValue(0, 0, queueGuid, 1);
                dmx.SetDmxValue(1, 0, queueGuid, 1);
                dmx.SetDmxValue(2, i, queueGuid, 1);
                dmx.SetDmxValue(7, 255, queueGuid, 1);

                System.Threading.Thread.Sleep(10);
            }
        }
    }
}
